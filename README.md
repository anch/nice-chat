REST api for chat application
 	- multithreading
 	- postgeSQL DB (json format storage)
 	- java 8 streams, lambda
 	- interaction with postgres
 	- FlyWay migration
 	- Generics
 	- Apache Kafka integration (fro messages queue)
 	- built with maven
 JUNIT 5 tests
 Docker container
 Kubernetes 
 

DB:
- Store messages
- Store users

Services:
- Authentication (Form auth)
- Kafka message queue
- Message service
- DB
- Front-end
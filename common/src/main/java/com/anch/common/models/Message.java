package com.anch.common.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Message {

    private Integer id;
    private String title;
    private String description;
    private Date creationTime;
    private User author;

}
